# VNC Viewer

A fork of [noVNC](https://github.com/noVNC/noVNC) with a changed theming, running at [Chimmie VNC](https://vnc.chimmie.k.vu).
