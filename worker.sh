#!/bin/sh

SCRIPT_PATH="$(dirname "$(realpath "$0")")"

cd "$SCRIPT_PATH"

cp res/img/icon.png .
cp res/img/icon.ico .
cp icon.png favicon.png
cp icon.ico favicon.ico
